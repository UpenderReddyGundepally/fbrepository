﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public interface iRule
    {
         bool ifMatches(int num);
         string doReplacement();
         //string getColour(int num);
    }
    
}
