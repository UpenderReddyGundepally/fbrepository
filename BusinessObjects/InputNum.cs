﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public class InputNum
    {
        [Required(ErrorMessage = "Please enter the number")]
        [Range(1, 500, ErrorMessage = "Enter number between 1 to 500")]
        public int inputNumber { get; set; }
    }
}
