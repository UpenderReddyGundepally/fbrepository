﻿using BusinessLogic;
using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Submit(InputNum objInputNum)
        {
            if (ModelState.IsValid)
            {
                FizzBuzzBL objFB = new FizzBuzzBL();
                List<OutputNum> objOutNumList = new List<OutputNum>();
                objOutNumList = objFB.ExecuteFB(objInputNum);
                return View("Result", objOutNumList);
            }
            else
            {
                return View("Index");
            }
            
        }
    }
}
