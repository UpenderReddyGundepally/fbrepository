﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class FizzBuzzRule : iRule
    {
        public bool ifMatches(int num)
        {
            return (num % 3 == 0) && (num % 5 == 0) ? true : false;
        }

        public string doReplacement()
        {
            return "Fizz Buzz";
        }
        //public string getColour(int num)
        //{

        //    return string.Empty;
        //}
    }
}
