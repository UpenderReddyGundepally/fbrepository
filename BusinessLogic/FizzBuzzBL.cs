﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class FizzBuzzBL
    {
        public List<OutputNum>  ExecuteFB(InputNum objInputNum)
        {
            
            FizzBuzz fizBuzz = new FizzBuzz();
            fizBuzz.addRule(new FizzBuzzRule());
            fizBuzz.addRule(new FizzRule());
            fizBuzz.addRule(new BuzzRule());

            fizBuzz.addColour(new FizzRule());
            fizBuzz.addColour(new BuzzRule());
           
            List<OutputNum> output = fizBuzz.generate(objInputNum.inputNumber);
            
            
            
            //List<OutputNum> objOutpurNum =new List<OutputNum>();
            //int inputNumber = objInputNum.inputNumber;
                

            //    for (int i = 1; i <= inputNumber; i++)
            //    {
            //        OutputNum r = new OutputNum();

            //        if (i % 3 == 0)
            //        {
            //            r.name = "fizz";
            //            r.labelColour = "blue";
            //            objOutpurNum.Add(r);

            //        }
            //        if (i % 5 == 0)
            //        {
            //            r.labelColour = "green";
            //            r.name = "buzz";
            //            objOutpurNum.Add(r);
            //        }

            //    }

            return output;                   

        }
    }
}
