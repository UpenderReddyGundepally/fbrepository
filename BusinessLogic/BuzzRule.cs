﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class BuzzRule : iRule,iColour
    {
        public bool ifMatches(int num)
        {
            return num % 5 == 0 ? true : false;
        }

        public string doReplacement()
        {
            return "Buzz";
        }
        //public string getColour(int num)
        //{
        //    if (num % 5 == 0)
        //        return "green";
        //    else
        //        return null;
        //}
        public string getColourNew(int num)
        {
            if (num % 5 == 0)
                return "green";
            else
                return null;
        }
    }
}
