﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLogic
{
    class FizzBuzz
    {
        List<iRule> iRule;
        List<iColour> iColour;
        public FizzBuzz()
        {
            iRule = new List<iRule>();
            iColour = new List<iColour>();
        }
        public List<OutputNum> generate(int num)
        {
            List<OutputNum> outputNumList = new List<OutputNum>();

            for (int i = 1; i <= num; i++)
            {
                OutputNum on = new OutputNum();
                on = doReplacement(i);
                outputNumList.Add(on);
                //outputList.Add(doReplacement(i));
            
            }
            return outputNumList;
        }

        public OutputNum doReplacement(int num)
        {
            OutputNum on = new OutputNum();
            foreach (var i in iRule)
            {
                if (i.ifMatches(num))
                {
                    on.number = i.doReplacement();
                    //on.labelcolour = i.getColour(num);


                    foreach (var i2 in iColour)
                    {
                        on.labelcolour = i2.getColourNew(num);
                        if (on.labelcolour != null)
                        {
                            break;
                        }
                    }
                    break;
                }
                else
                {
                    on.number = num.ToString();
                }
                
            }
            return on;
        }

        public void addRule(iRule ir)
        {
            iRule.Add(ir);
        }
        public void addColour(iColour ic)
        {
            iColour.Add(ic);
        }
    }
}
