﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class FizzRule : iRule,iColour
    {
        public bool ifMatches(int num)
        {
            return num % 3 == 0 ? true : false;
        }

        public string doReplacement()
        {
            return "Fizz";
        }


        //public string getColour(int num)
        //{
        //    if (num % 3 == 0)
        //        return "blue";
        //    else
        //        return string.Empty;
        //}
        public string getColourNew(int num)
        {
            if (num % 3 == 0)
                return "blue";
            else
                return null;
        }
    }

    

}
